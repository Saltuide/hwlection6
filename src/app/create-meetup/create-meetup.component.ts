import { Component } from '@angular/core';
import { MeetupService } from '../meetup.service';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';


@Component({
  selector: 'app-create-meetup',
  templateUrl: './create-meetup.component.html',
  styleUrls: ['./create-meetup.component.scss']
})
export class CreateMeetupComponent {

  constructor(private _meetupService: MeetupService) { }

  /**
   * Метод обрабатывающий введенные данные в форму
   * @param form - форма
   */
  submitForm() {
    console.log(this.createForm.value);
  }
  private dateValidator(): ValidatorFn{
    const pattern: RegExp = /^([1]{1}[0-2]{1}|[0]{1}[0-9]{1})[.-/]{1}([0-2]{1}[0-9]|[3]{1}[0-1])[.-/]{1}((?:[0-9]{2})?[0-9]{2}$)/;
    return (control: AbstractControl): {[key: string] :any}=>{
      if(!(control.dirty || control.touched)){
        return null;
      }else{
        return pattern.test(control.value) ? null : {custom :`Invalid date`};

      }
    };
  }
  createForm = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.maxLength(10),
      Validators.minLength(2),
    ]),
    address: new FormControl('', [
      Validators.required,
      Validators.maxLength(30),
      Validators.minLength(2),
    ]),
    description: new FormControl('', [
      Validators.required,
      Validators.maxLength(50),
      Validators.minLength(2),
    ]),
    from_date: new FormControl('', [
      Validators.required,
      this.dateValidator
    ]),
    published: new FormControl(false),
  })
  ngOnInit() {
     /*  this.createForm.get('name').valueChanges.subscribe(value => {
        console.log('name value', value);

      this.createForm.get('published').valueChanges.subscribe(value => {
        if (value) {
          this.createForm.get('name').enable();
        } else {
          this.createForm.get('name').disable();
        }
      });

  });
  */
  }
}
